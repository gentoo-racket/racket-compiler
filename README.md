# Racket Directory Compiler

<p align="center">
    <a href="https://archive.softwareheritage.org/browse/origin/?origin_url=https://gitlab.com/gentoo-racket/racket-compiler/">
        <img src="https://archive.softwareheritage.org/badge/origin/https://gitlab.com/gentoo-racket/racket-compiler/">
    </a>
    <a href="https://gitlab.com/gentoo-racket/racket-compiler/pipelines">
        <img src="https://gitlab.com/gentoo-racket/racket-compiler/badges/master/pipeline.svg">
    </a>
</p>


## About

Small tool to compile Racket source code.


## License

This file is part of racket-compiler.

racket-compiler is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3.

racket-compiler is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with racket-compiler.  If not, see <https://www.gnu.org/licenses/>.

Original author: Maciej Barć <xgqt@riseup.net>
Copyright (c) 2021-2022, Maciej Barć <xgqt@riseup.net>
Licensed under the GNU GPL v2 License
SPDX-License-Identifier: GPL-2.0-or-later
